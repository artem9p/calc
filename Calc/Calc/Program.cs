﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Calc
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Введите формулу дупустимиэ знаки /*+-()^ без пробелов з точкой:");
                //   string mystring = "50+5*(2-(5,5+2)*(15*60+5)*15*(5+10))*50/5+5^12"; 
                string mystring = Console.ReadLine();
                mystring = mystring.Replace('.', ',');
                Console.WriteLine("Результат");
                Console.WriteLine(Calculate(mystring));
            }
        }
        public static string FindSimplLine(string Stroka)
        {
            Match m = Regex.Match(Stroka, @"\([^\(,\)]*\)");    //поиск ровних дужок(*)
            if (m.Success)
            {
                string rez = Convert.ToString(Calculate(Convert.ToString(m.Value).Substring(1, Convert.ToString(m.Value).Length - 2)));
                Stroka = Stroka.Substring(0, m.Index) + rez + Stroka.Substring(m.Index + m.Length);
                Stroka = FindSimplLine(Stroka);
            }
            return Stroka;
        }
        public static double Calculate(string SimplLine)// калькуляция списка без дужок
        {
            SimplLine = FindSimplLine(SimplLine);
            Console.WriteLine(SimplLine);				//отладака
            List<string> s = new List<String>(); 		//s - списсок аргументов строки
            string b = null;
            for (int i = 0; i < SimplLine.Length; i++)
            {
                if (SimplLine[i] == '+' || SimplLine[i] == '-' ||
                    SimplLine[i] == '*' || SimplLine[i] == '/' || SimplLine[i] == '^')
                {
                    if (b != null) s.Add(b);
                    s.Add(Convert.ToString(SimplLine[i]));
                    b = null;
                }
                else
                {
                    b = b + SimplLine[i];
                }
                if (i == SimplLine.Length - 1) { s.Add(b); }

            }

            int nomer;
            double firstArgument;
            double secondArgument;
            string Rezyltat;
            do
            {
                if (s.Exists(a => a == "*"))
                {
                    nomer = s.FindLastIndex(a => a == "*");
                    firstArgument = Convert.ToDouble(s[nomer - 1]);
                    secondArgument = Convert.ToDouble(s[nomer + 1]);
                    Rezyltat = Convert.ToString(firstArgument * secondArgument);
                    s.RemoveRange(nomer - 1, 3);
                    s.Insert(nomer - 1, Rezyltat);
                };
                if (s.Exists(a => a == "/"))
                {
                    nomer = s.FindLastIndex(a => a == "/");
                    firstArgument = Convert.ToDouble(s[nomer - 1]);
                    secondArgument = Convert.ToDouble(s[nomer + 1]);
                    Rezyltat = Convert.ToString(firstArgument / secondArgument);
                    s.RemoveRange(nomer - 1, 3);
                    s.Insert(nomer - 1, Rezyltat);
                };
            }
            while (s.Exists(a => a == "*") || s.Exists(a => a == "/"));
            do
            {
                if (s.Exists(a => a == "^"))
                {
                    nomer = s.FindLastIndex(a => a == "^");
                    firstArgument = Convert.ToDouble(s[nomer - 1]);
                    secondArgument = Convert.ToDouble(s[nomer + 1]);
                    Rezyltat = Convert.ToString(Math.Pow(firstArgument, secondArgument));
                    s.RemoveRange(nomer - 1, 3);
                    s.Insert(nomer - 1, Rezyltat);
                };
            }
            while (s.Exists(a => a == "^"));
            do
            {
                if (s.Exists(a => a == "+"))
                {
                    nomer = s.FindLastIndex(a => a == "+");
                    firstArgument = Convert.ToDouble(s[nomer - 1]);
                    secondArgument = Convert.ToDouble(s[nomer + 1]);
                    Rezyltat = Convert.ToString(firstArgument + secondArgument);
                    s.RemoveRange(nomer - 1, 3);
                    s.Insert(nomer - 1, Rezyltat);
                };
                if (s.Exists(a => a == "-"))
                {
                    nomer = s.FindLastIndex(a => a == "-");
                    firstArgument = Convert.ToDouble(s[nomer - 1]);
                    secondArgument = Convert.ToDouble(s[nomer + 1]);
                    Rezyltat = Convert.ToString(firstArgument - secondArgument);
                    s.RemoveRange(nomer - 1, 3);
                    s.Insert(nomer - 1, Rezyltat);
                };
            }
            while (s.Exists(a => a == "+") || (s.Exists(a => a == "-")));
            return Convert.ToDouble(s[0]);
        }
    }
}
